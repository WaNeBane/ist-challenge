create table tbl_user (
       id int8 not null,
        created_at timestamp not null,
        password varchar(25) not null,
        role varchar(20),
        updated_at timestamp,
        username varchar(25) not null,
        primary key (id)
)
create sequence user_sequence start 1 increment 1
create index OrderByLastDate on tbl_user (created_at, updated_at desc)
alter table tbl_user
       add constraint UniqueUserData unique (username)