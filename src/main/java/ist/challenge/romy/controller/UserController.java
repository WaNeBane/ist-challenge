package ist.challenge.romy.controller;

import ist.challenge.romy.dto.ResponseSuccess;
import ist.challenge.romy.dto.UserUpdateRequest;
import ist.challenge.romy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/list")
    public ResponseEntity<ResponseSuccess> getAllUsers(){
        return ResponseEntity.ok(service.getAllUsers());
    }

    @PutMapping("/edit")
    public ResponseEntity<ResponseSuccess> updateUser(
            @Valid @RequestBody UserUpdateRequest request){
        return new ResponseEntity<>(service.updateUser(request), HttpStatus.CREATED);
    }
}
