package ist.challenge.romy.controller;

import ist.challenge.romy.dto.RegistrationRequest;
import ist.challenge.romy.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/register")
public class RegistrationController {

    @Autowired
    private RegistrationService service;

    @PostMapping("/user")
    public ResponseEntity<?> registrationUser(
            @Valid @RequestBody RegistrationRequest request){
        return new ResponseEntity<>(service.registration(request), HttpStatus.CREATED);
    }
}