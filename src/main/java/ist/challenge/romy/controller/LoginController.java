package ist.challenge.romy.controller;

import ist.challenge.romy.dto.LoginRequest;
import ist.challenge.romy.dto.ResponseSuccess;
import ist.challenge.romy.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/")
    public ResponseEntity<ResponseSuccess> userLogin(
            @Valid @RequestBody LoginRequest request) {
        return ResponseEntity.ok(loginService.userLogin(request));
    }
}
