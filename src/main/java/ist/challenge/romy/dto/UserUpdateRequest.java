package ist.challenge.romy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateRequest {

    @NotEmpty(message = "Email Lama Tidak Boleh Kosong")
    @Size(min = 5, max = 25, message = "Karakter Email Lama Min 5, Max 25")
    private String oldEmail;
    @NotEmpty(message = "Password Lama Tidak Boleh Kosong")
    @Size(min = 8, max = 25, message = "Karakter Password Lama Min 8, Max 25")
    private String oldPassword;
    @NotEmpty(message = "Email Baru Tidak Boleh Kosong")
    @Size(min = 5, max = 25, message = "Karakter Email Baru Min 5, Max 25")
    private String newEmail;
    @NotEmpty(message = "Password Baru Tidak Boleh Kosong")
    @Size(min = 8, max = 25, message = "Karakter Password Baru Min 8, Max 25")
    private String newPassword;
}
