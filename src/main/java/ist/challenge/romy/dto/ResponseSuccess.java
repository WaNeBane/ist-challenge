package ist.challenge.romy.dto;

public class ResponseSuccess extends ResponseService{
    private static final String msgSuccess = "%s Data Success";
    public ResponseSuccess(String responseCode, Object responseDescription, Object data) {
        super(responseCode, String.format(msgSuccess, responseDescription), data);
    }
}
