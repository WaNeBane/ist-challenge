package ist.challenge.romy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationRequest {

    @NotEmpty(message = "Email Tidak Boleh Kosong")
    @Size(min = 5, max = 25, message = "Karakter Email Min 5, Max 25")
    private String email;
    @NotEmpty(message = "Password Tidak Boleh Kosong")
    @Size(min = 8, max = 25, message = "Karakter Password Min 8, Max 25")
    private String password;
}
