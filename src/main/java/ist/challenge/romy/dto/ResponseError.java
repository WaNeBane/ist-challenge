package ist.challenge.romy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseError {
    private String statusCode;
    private String message;
    private String errorReason;
}
