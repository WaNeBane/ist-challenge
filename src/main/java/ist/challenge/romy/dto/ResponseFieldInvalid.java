package ist.challenge.romy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseFieldInvalid{

    private String errorCode;
    private Object solution;
    private Object description;
}
