package ist.challenge.romy.util;

public enum MessageRes {
    UPDATE("Update"),
    SAVED("Saved"),
    GET("Get"),
    GET_LIST("Get List"),
    LOGIN_SUCCESS("Login Success");

    public final String name;
    private MessageRes(String name) {
        this.name = name;
    }
}
