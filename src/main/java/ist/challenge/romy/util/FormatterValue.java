package ist.challenge.romy.util;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class FormatterValue {

    private static final String fmtDateTime = "yyyy-MM-dd HH:mm:ss";
    private static final SimpleDateFormat sdf = new SimpleDateFormat(fmtDateTime);

    public String convertLocalDateToString(LocalDateTime date){
        return date!=null ? sdf.format(convertLocalDate(date)) : null;
    }

    private static Date convertLocalDate(LocalDateTime localDateTime){
        return Date.from(localDateTime.atZone(zoneId()).toInstant());
    }

    private static ZoneId zoneId(){
        return ZoneId.systemDefault();
    }
}
