package ist.challenge.romy.util;


import ist.challenge.romy.dto.UserResponse;
import ist.challenge.romy.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataMapper {

    @Autowired
    private FormatterValue formatterValue;

    public UserResponse generateUserDto(User user){
        return UserResponse.builder()
                .email(user.getUsername())
                .createdAt(formatterValue.convertLocalDateToString(user.getCreatedAt()))
                .updatedAt(formatterValue.convertLocalDateToString(user.getUpdatedAt()))
                .build();
    }

    public List<UserResponse> generateListUserDto(List<User> users){
        return users.stream()
                .map(this::generateUserDto)
                .collect(Collectors.toList());
    }
}
