package ist.challenge.romy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomyApplication {

	public static void main(String[] args) {
		SpringApplication.run(RomyApplication.class, args);
	}

}
