package ist.challenge.romy.exception;

import ist.challenge.romy.dto.ResponseError;
import ist.challenge.romy.dto.ResponseFieldInvalid;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalErrorException extends ResponseEntityExceptionHandler {

    private static final String MSG_DATA_BAD_REQUEST = "Please check again!!";
    private static final String MSG_DATA_EXISTS = "Already Exists!";
    private static final String MSG_NOT_MATCH = "Data not match!";
    private static final String MSG_PASSWORD_NOT_DIFF = "Buat password yang berbeda dengan sebelumnya!";
//    private static final String USERNAME_EXISTS = "Email user %s sudah terpakai";
    private static final String USERNAME_NOT_MATCH = "Email / password tidak cocok";
    private static final String USERNAME_PASSWORD_NOT_DIFF = "Password tidak boleh sama dengan sebelumnya";

    private List<String> errorDescription(BindingResult result){
        return result.getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        return new ResponseEntity<>(
                ResponseFieldInvalid.builder()
                        .errorCode(String.valueOf(HttpStatus.BAD_REQUEST.value()))
                        .solution(MSG_DATA_BAD_REQUEST)
                        .description(errorDescription(ex.getBindingResult()))
                        .build(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameAlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ResponseError> handleUsernameAlreadyExists(UsernameAlreadyExistException ex){
        return new ResponseEntity<>(
                ResponseError.builder()
                        .statusCode(String.valueOf(HttpStatus.CONFLICT.value()))
                        .message(MSG_DATA_EXISTS)
                        .errorReason(ex.getMessage())
                        .build(),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler(UsernameNotMatchException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ResponseError> handleUsernameOrPasswordIsNotMatch(UsernameNotMatchException ex){
        return new ResponseEntity<>(
                ResponseError.builder()
                        .statusCode(String.valueOf(HttpStatus.UNAUTHORIZED.value()))
                        .message(MSG_NOT_MATCH)
                        .errorReason(USERNAME_NOT_MATCH)
                        .build(),
                HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(UsernamePasswordMustBeDifferentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ResponseError> handleUsernamePasswordNotDifferent(UsernamePasswordMustBeDifferentException ex){
        return new ResponseEntity<>(
                ResponseError.builder()
                        .statusCode(String.valueOf(HttpStatus.BAD_REQUEST.value()))
                        .message(MSG_PASSWORD_NOT_DIFF)
                        .errorReason(USERNAME_PASSWORD_NOT_DIFF)
                        .build(),
                HttpStatus.BAD_REQUEST);
    }
}
