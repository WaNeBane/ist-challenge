package ist.challenge.romy.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsernameNotMatchException extends RuntimeException {
    public UsernameNotMatchException(String idClass, Throwable cause, boolean enableSuppresion, boolean writableStackTrace){
        super(idClass, cause, enableSuppresion, writableStackTrace);
    }
}
