package ist.challenge.romy.service;

import ist.challenge.romy.config.JwtService;
import ist.challenge.romy.dto.RegistrationRequest;
import ist.challenge.romy.dto.RegistrationResponse;
import ist.challenge.romy.dto.ResponseSuccess;
import ist.challenge.romy.util.MessageRes;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegistrationService {
    private UserService userService;
    private JwtService jwtService;

    public ResponseSuccess registration(RegistrationRequest request) {
        String jwtToken = jwtService.generateToken(userService.generateUserRegistration(request.getEmail(), request.getPassword()));
        return new ResponseSuccess(
                String.valueOf(HttpStatus.CREATED.value()),
                MessageRes.SAVED.name,
                RegistrationResponse.builder()
                        .email(request.getEmail())
                        .token(jwtToken)
                        .build()
        );
    }
}
