package ist.challenge.romy.service;

import ist.challenge.romy.config.JwtService;
import ist.challenge.romy.dto.LoginRequest;
import ist.challenge.romy.dto.RegistrationResponse;
import ist.challenge.romy.dto.ResponseSuccess;
import ist.challenge.romy.entity.User;
import ist.challenge.romy.exception.UsernameNotMatchException;
import ist.challenge.romy.util.MessageRes;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LoginService{
    private UserService userService;
    private JwtService jwtService;

    public ResponseSuccess userLogin(LoginRequest request) {
        User user = userService.findByUsername(request.getEmail());

        if(!request.getPassword().equals(user.getPassword())){
            throw new UsernameNotMatchException();
        }

        String jwtToken = jwtService.generateToken(user);
        return new ResponseSuccess(
                String.valueOf(HttpStatus.OK.value()),
                MessageRes.SAVED.name,
                RegistrationResponse.builder()
                        .email(request.getEmail())
                        .token(jwtToken)
                        .build()
        );
    }
}
