package ist.challenge.romy.service;

import ist.challenge.romy.dto.ResponseSuccess;
import ist.challenge.romy.dto.UserUpdateRequest;
import ist.challenge.romy.entity.User;
import ist.challenge.romy.exception.UsernameAlreadyExistException;
import ist.challenge.romy.exception.UsernamePasswordMustBeDifferentException;
import ist.challenge.romy.repository.UserRepository;
import ist.challenge.romy.util.DataMapper;
import ist.challenge.romy.util.MessageRes;
import ist.challenge.romy.util.Role;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService{

    private UserRepository userRepository;

    private void checkIfUserExist(String username){
        userRepository.findByUsername(username)
                .ifPresent(user -> {
                    throw new UsernameAlreadyExistException(user.getUsername());
                });
    }

    private User update(UserUpdateRequest request){

        userRepository.findByUsername(request.getNewEmail())
                .ifPresent(user -> {
                    throw new UsernameAlreadyExistException(user.getUsername());
                });

        User oldUser = findByUsername(request.getOldEmail());
        if(oldUser.getPassword().equals(request.getNewPassword())){
            throw new UsernamePasswordMustBeDifferentException();
        }

        User newUser = User.builder()
                .id(oldUser.getId())
                .username(request.getNewEmail())
                .password(request.getNewPassword())
                .updatedAt(LocalDateTime.now())
                .build();

        create(newUser);

        return newUser;
    }
    public User generateUserRegistration(String username, String password){
        Boolean userExists = userRepository.checkExistsUsername(username);
        if(userExists){
            throw new UsernameAlreadyExistException("Email user "+username+" sudah terpakai");
        }
        User user =  User.builder()
                .username(username)
                .password(password)
                .createdAt(LocalDateTime.now())
                .role(Role.USER)
                .build();
        create(user);
        return user;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public User create(User data) {
        return userRepository.save(data);
    }

    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public ResponseSuccess getAllUsers(){
        return new ResponseSuccess(
                String.valueOf(HttpStatus.OK.value()),
                MessageRes.GET_LIST.name,
                new DataMapper().generateListUserDto(findAllUsers())
        );
    }

    public ResponseSuccess updateUser(UserUpdateRequest request) {
        return new ResponseSuccess(
                String.valueOf(HttpStatus.OK.value()),
                MessageRes.UPDATE.name,
                new DataMapper().generateUserDto(update(request))
        );
    }

}
