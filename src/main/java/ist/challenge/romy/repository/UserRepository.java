package ist.challenge.romy.repository;

import ist.challenge.romy.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    @Query("SELECT CASE WHEN COUNT(s) > 0 " +
            "THEN TRUE ELSE FALSE END " +
            "FROM User s " +
            "WHERE s.username = :username")
    Boolean checkExistsUsername(String username);
}
