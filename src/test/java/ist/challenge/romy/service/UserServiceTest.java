package ist.challenge.romy.service;

import ist.challenge.romy.entity.User;
import ist.challenge.romy.exception.UsernameAlreadyExistException;
import ist.challenge.romy.repository.UserRepository;
import ist.challenge.romy.util.Role;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    private UserService userServiceTest;

    @BeforeEach
    void setUp() {
        userServiceTest = new UserService(userRepository);
    }

    @Test
    @DisplayName("Get All Users")
    void canGetAllUsers(){
        // when
        userServiceTest.findAllUsers();
        verify(userRepository).findAll();
    }

    @Test
    @DisplayName("User with username & password is filled in")
    void addUserWithFieldIsFilledIn(){
        //given
        String username = "sample@gmail.com";
        String password = "samplepassword123";

        User user = User.builder()
                .username(username)
                .password(password)
                .createdAt(LocalDateTime.now())
                .role(Role.USER)
                .build();

        userServiceTest.generateUserRegistration(username, password);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());

        User captureUser = userArgumentCaptor.getValue();

        assertThat(captureUser.getUsername()).isEqualTo(user.getUsername());
    }

    @Test
//    @Disabled
    @DisplayName("Will throw exception if email is already exists")
    void addUserWillThrowWhenEmailIsExists(){
        String username = "bane@gmail.com";
        String password = "passwordbane";
        User user = User.builder()
                .username(username)
                .password(password)
                .createdAt(LocalDateTime.now())
                .role(Role.USER)
                .build();

        given(userRepository.checkExistsUsername(user.getUsername()))
                .willReturn(true);

        assertThatThrownBy(() -> userServiceTest.generateUserRegistration(username, password))
                .isInstanceOf(UsernameAlreadyExistException.class)
                .hasMessage("Email user "+username+" sudah terpakai")
        ;
    }

    @AfterEach
    void tearDown() {

    }
}