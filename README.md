# IST Challenge

Login & Registration backend using Spring Boot

## Features
* User registration and login with JWT Authentication
* Customized access denied handling

## Technologies
- [x] Spring Boot 2
- [x] Java 11
- [x] PostgresSQL
- [x] Lombok
- [x] Spring Security
- [x] JSON Web Tokens (JWT)


## Example Request

### Postman

### CURL
```
curl --location --request POST 'localhost:8080/api/v1/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "sample-email@email.com",
    "password": "password"
}'
```